# Test Python Project
A simple repo showing best practices for setting up any simple Python project.

Includes:
- Git setup
- Always document a README
- Python package structure
- Using `poetry` for Python dependencies
- Some simple code to show package structure
- Pytest with simple unit testing
- A gitignore
- Simply Gitlab CI setup

